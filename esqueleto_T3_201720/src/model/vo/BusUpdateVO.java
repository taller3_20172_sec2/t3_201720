package model.vo;

import com.google.gson.JsonObject;

public class BusUpdateVO<T> {
	
	
	private String vehicleNo;
	private String tripID;
	private String routeNo;
	private String direction;
	private String destination;
	private String pattern;
	private double latitude;
	private double longitud;
	private String recordedTime;
	private JsonObject routeMap;
	
	public BusUpdateVO(String pVehicleNo, String pTripID, String pRouteNo, String pDirection, 
			String pDestination, String pPattern, Double pLatitude, Double pLongitud, 
			String pRecordedTime, JsonObject pRouteMap)
	{
		vehicleNo = pVehicleNo;
		tripID = pTripID;
		routeNo = pRouteNo;
		direction = pDirection;
		destination = pDestination;
		pattern = pPattern;
		latitude = pLatitude;
		longitud = pLongitud;
		recordedTime = pRecordedTime;
		routeMap = pRouteMap;
	}
	
	public String getVehicleNo() {
		return vehicleNo;
	}



	public String getTripID() {
		return tripID;
	}


	public String getDirection() {
		return direction;
	}




	public String getDestination() {
		return destination;
	}



	public String getPattern() {
		return pattern;
	}


	public double getLatitude() {
		return latitude;
	}


	public double getLongitud() {
		return longitud;
	}

	public String getRecordedTime() {
		return recordedTime;
	}


	public JsonObject getRouteMap() {
		return routeMap;
	}
}
