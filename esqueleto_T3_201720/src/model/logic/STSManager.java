package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;

import model.data_structures.Queue;
import model.data_structures.IStack;
import model.data_structures.Iterator;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import model.vo.VOStop;
import api.ISTSManager;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class STSManager<T> implements ISTSManager{

	private JsonParser json;
	private Queue colaBus;
	private Queue Stops;
	private Queue tripIds;
	File[] files = new File("").listFiles();
	
	@Override
	public void readBusUpdate(File rtFile) {
		// TODO Auto-generated method stub
		json = new JsonParser();

		try
		{
			Object par = json.parse(new FileReader(rtFile));
			JsonObject bus = (JsonObject) par;


			String vehicleNo = (bus.get("VehicleNo")).getAsString();
			String tripID = bus.get("TripId").getAsString();
			String routeNo = bus.get("RouteNo").getAsString();
			String direction = bus.get("Direction").getAsString();
			String destination = bus.get("destination").getAsString();
			String pattern = bus.get("Pattern").getAsString();
			double latitude = bus.get("Latitude").getAsDouble();
			double longitud = bus.get("Longitud").getAsDouble();
			String recordedTime = bus.get("RecordedTime").getAsString();
			JsonObject routeMap = bus.get("RouteMap").getAsJsonObject();

			
			Iterator<String> keys =  (Iterator<String>) bus.keySet().iterator();
			while(keys.hasNext())
			{
				BusUpdateVO<BusUpdateVO> nuevo = new <BusUpdateVO>BusUpdateVO(vehicleNo, tripID, routeNo, direction, destination, pattern, latitude, longitud, recordedTime, routeMap);
				colaBus.enqueue(nuevo);
			
			
			}
		}
			catch(Exception e)
			{
				
			}
		
		}

		public IStack<VOStop> listStops(Integer tripID) {
		Stack<VOStop> ans= null;
		List iDList= null;
		Integer iDActual=null;
		Queue tripsidlist = new Queue<JsonElement>();
		Iterator it=tripsidlist.iterator();
		Node<VOStop> nodeId =  colaBus.peek();
		while(nodeId!=null)
		{
			VOStop current = nodeId.getData();
			Integer currentTripId = current.getId();
			if(currentTripId==tripID)
			{
				Node<VOStop> toCompare =  colaBus.peek().darSiguiente();
				VOStop newCurrent = nodeId.getData();
				Integer newCurrentTripId = newCurrent.getId();
				double latCurrent=current.getStopLat();
				double lonCurrent=current.getStopLon();
				while(toCompare!=null)
				{
					double latNewCurrent=current.getStopLat();
					double lonNewCurrent=current.getStopLon();
					if(getDistance(latCurrent, lonCurrent, latNewCurrent, lonNewCurrent)<=70)
					{
						ans.push(nodeId.getData());
					}

					toCompare=toCompare.darSiguiente();
				}
			}
			nodeId=nodeId.darSiguiente();
		}

		return ans;
	}

	
	public void loadStops() {
		// TODO Auto-generated method stub
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/stops.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");

				int id = Integer.parseInt(data[0]);
				int code= Integer.parseInt(data[1]);
				String name = data[2];
				String stopDesc = data[3];
				double stopLat = Double.parseDouble(data[4]);
				double stopLon = Double.parseDouble(data[5]);
				String stopZone = data[6];
				URL stopUrl = new URL(data[7]);
				int locationType = Integer.parseInt(data[8]);
				String parentStation = data[9];

				Stops.enqueue(new VOStop(id, code, name, stopDesc, stopLat, stopLon, stopZone, stopUrl, locationType, parentStation));

				line = reader.readLine();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}
	
	
}
