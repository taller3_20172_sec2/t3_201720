package model.data_structures;

public class Queue<T> implements IQueue<T>{

	private Node<T> first;
	private Node<T> last;
	private int n;

	public Queue() {
		first = null;
		last  = null;
		n = 0;
	}


	public boolean isEmpty() {
		return first == null;
	}

	public int size() {
		return n;
	}

	public T peek() {
		T ans=null;
		if (!isEmpty()) 
			ans= first.getData();
		return ans;
	}

	public void enqueue(T item) {
		Node<T> oldlast = last;
		last = new Node<T>(item);
		last.setData(item);
		last.siguiente = null;
		if (isEmpty()) 
			first = last;
		else   
			oldlast.siguiente = last;
		n++;
	}

	public T dequeue() {
		T ans=null;
		if (!isEmpty()){
		T a = first.getData();
		first = first.siguiente;
		ans=a;
		n--;
		}
		if (isEmpty()) last = null;   // to avoid loitering
		return ans;
	}

	public Iterator<T> iterator()  {
		return new ListIterator<T>(first);  
	}


}
