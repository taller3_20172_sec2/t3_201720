package model.data_structures;

public class ListIterator<Item> implements Iterator<Item> {
    private Node<Item> current;

    public ListIterator(Node<Item> first) {
        current = first;
    }

    public boolean hasNext() {
        return current != null;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public Item next() {
    	Item ans=null;
        if (hasNext()) {
        Item item = current.getData();
        current = current.siguiente; 
        ans= item;
        }
		return ans;
    }
}
