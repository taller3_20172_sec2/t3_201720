package testDataStructures;

import model.data_structures.Stack;
import junit.framework.TestCase;



public class stackTest<T> extends TestCase {
	Stack <String> stack = new Stack<String>();
	private String cadena = "";
	
	
	public void SetUp()
	{
		int size = 5;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			stack.push(cadena);
		}
	}
	
	public void SetUp2()
	{
		int size = 5;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			stack.push(cadena);
		}
		stack.push("toPop");
	}
	
	
	public void pushTest()
	{
		SetUp();
		stack.push("pushTest");
		assertEquals("pushTest", stack.peek());
	}
	
	public void popTest()
	{
		SetUp2();
		assertEquals("toPop", stack.pop());	
	}
	
	public void cantidadElementos()
	{
		SetUp();
		assertEquals(5, stack.size());
	}
	
	public void isEmptyTest()
	{
		SetUp();
		assertEquals(false, stack.isEmpty());
	}
}
