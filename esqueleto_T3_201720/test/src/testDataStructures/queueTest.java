package testDataStructures;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class queueTest<T> extends TestCase{
	
	Queue<String> colaNueva = new Queue<String>();
	private String cadena = "";
	
	//Escenario 1
	public void SetUp()
	{
		int size = 5;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			colaNueva.enqueue(cadena);
		}
	}
	
	//Escenario 2
	public void SetUp2()
	{
		int size = 5;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			colaNueva.enqueue(cadena);
		}
		colaNueva.enqueue("dequeue");
	}
	
	public void enqueueTest()
	{
		SetUp();
		colaNueva.enqueue("enqueueTest");
		assertEquals("enqueueTest", colaNueva.peek());
	}
	
	public void dequeueTest()
	{
		SetUp2();
		assertEquals("dequeue", colaNueva.dequeue());
	}
	
	public void cantidadElementosTest()
	{
		SetUp();
		assertEquals(5, colaNueva.size());
	}
	
	public void isEmptyTest()
	{
		SetUp();
		assertEquals(false, colaNueva.isEmpty());
	}
	
	
}
